package com.example.ft290lifecycleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    TextView textView;
    StringBuilder stringBuider = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        showLifeCyleMessage("onCreate()");

    }

    @Override
    protected void onStart() {
        super.onStart();
        showLifeCyleMessage("onStart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        showLifeCyleMessage("onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLifeCyleMessage("onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showLifeCyleMessage("onRestart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        showLifeCyleMessage("onStop()");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showLifeCyleMessage("onDestroy()");
    }

    private void showLifeCyleMessage(String fase) {
        textView.setText(stringBuider.append(fase).append("\n"));
        Log.d(TAG, String.format("Executou [%s].",fase));
    }
}
